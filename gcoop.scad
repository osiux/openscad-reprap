// This script comes with ABSOLUTELY NO WARRANTY, use at own risk
// Copyright (C) 2015 Osiris Alejandro Gomez <osiris@gcoop.coop>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// espesor
h=6;

// radios
r1=1.4;
r2=(1.85/1.4)*r1;
r3=(2.20/1.85)*r2;
r4=(2.84/2.20)*r3;
r5=(3.55/2.84)*r4;

// distancias
d1=20.45;
d2=(25.55/20.45)*d1;
d3=(31.95/25.55)*d2;
d4=(39.95/31.95)*d3;
d5=(50/39.95)*d4;

// angulos
a1=15;
a2=30;

gcoop_x=d5+r5*2;

module puntos() {
    $fn=16;
    rotate([0,0,a1]) translate([d1,0,0]) cylinder(r=r1, h);
    rotate([0,0,a2]) translate([d2,0,0]) cylinder(r=r2, h);
    $fn=18;
    rotate([0,0,a1]) translate([d3,0,0]) cylinder(r=r3, h);
    $fn=24;
    rotate([0,0,a2]) translate([d4,0,0]) cylinder(r=r4, h);
    $fn=32;
    rotate([0,0,a1]) translate([d5,0,0]) cylinder(r=r5, h);
}

module gcoop_isotipo() {
    for ( i = [1 : 12] ) {
        translate([gcoop_x, gcoop_x, -1]) rotate([0,0,i*a2]) puntos();
    }
}

module base_circulo() {
    $fn=128;
    cylinder(r=d5+r5*2, h-2);
}

module gcoop_circulo() {

    difference() {
        translate([gcoop_x, gcoop_x,  0]) base_circulo();
        gcoop_isotipo();
    }

}

module gcoop_cuadrado() {

    difference() {
        cube([120, 120,  h-2]);
        translate([2,2,0]) gcoop_isotipo();
    }

}

gcoop_cuadrado();

