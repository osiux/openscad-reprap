// This script comes with ABSOLUTELY NO WARRANTY, use at own risk
// Copyright (C) 2015 Osiris Alejandro Gomez <osiris@gcoop.coop>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

module puntos() {
	rotate([0,0,a1]) translate([d1,0,0]) cylinder(r=r1, h);
	rotate([0,0,a2]) translate([d2,0,0]) cylinder(r=r2, h);
	rotate([0,0,a1]) translate([d3,0,0]) cylinder(r=r3, h);
	rotate([0,0,a2]) translate([d4,0,0]) cylinder(r=r4, h);
	rotate([0,0,a1]) translate([d5,0,0]) cylinder(r=r5, h);
}
